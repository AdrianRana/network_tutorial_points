﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SphereSpawner : NetworkBehaviour
{
    public GameObject ballPrefab;
    public Transform spawnPoint;

  

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            GameObject ball = (GameObject)Instantiate(ballPrefab, new Vector3(spawnPoint.position.x, spawnPoint.position.y, spawnPoint.position.z), new Quaternion(0, 0, 0, 0));
            NetworkServer.Spawn(ball);
        }
    }
}
