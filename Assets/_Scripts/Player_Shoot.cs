﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Player_Shoot : NetworkBehaviour
{
    public GameObject hitEffect;
    public Transform firstPersonCharacter;
    private RaycastHit hit;
    private int damage = 20;
    Text txtPoints;
    PointCounter myCounter;

    public List<GameObject> players = new List<GameObject>();

    [Header("POINTS")]
    [SyncVar]public int points = 100;
    public int tempPoints = 100;
    
    void Start()
    {
        //myCounter = GameObject.Find("PointCounter").GetComponent<PointCounter>();
        if (!txtPoints)
        {
            txtPoints = GameObject.Find("worldPoints").GetComponent<Text>();
        }

    }

    void Update()
    {
        if(isLocalPlayer && Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
        //txtPoints.text = points.ToString();
        print("Update: " + points);

        GameObject.FindGameObjectsWithTag("Player");

    }

    //public override void OnStartServer()
    //{
    //    points = 0;
    //}


    void OnPointAdded(int value)
    {
        points = value;
        txtPoints.text = points.ToString();
    }


    void Shoot()
    {
        if(Physics.Raycast(firstPersonCharacter.transform.position, firstPersonCharacter.transform.forward, out hit))
        {
            Quaternion hitAngle = Quaternion.LookRotation(hit.normal);


            
            CmdSpawnhitPrefab(hit.point, hitAngle);
                

            //CmdAddPoint();
            tempPoints--;
            

            if (hit.transform.CompareTag("Player")) 
            {
                //Disparando a otro player
                CmdApplyDamageOnServer(hit.transform.GetComponent<NetworkIdentity>().netId);
            }
        }
    }

    [ClientRpc]
    void RpcAddPoint()
    {
        //OnPointAdded(tempPoints);        
        points--;
        txtPoints.text = points.ToString();   
    }

    [Command]
    void CmdSpawnhitPrefab(Vector3 pos, Quaternion rot)
    {
        print("Estoy en el Command");
        GameObject hitEffectGo = (GameObject)Instantiate(hitEffect, pos, rot);      //Spawnea en el server
        NetworkServer.Spawn(hitEffectGo);                                           //Spawnea en todo el network (los clientes supongo)

        

        if (isServer)
        {
            txtPoints.text = points.ToString();
        }
        RpcAddPoint();

    }

    [Command]
    void CmdApplyDamageOnServer(NetworkInstanceId networkID)
    {
        GameObject hitPlayerGo = NetworkServer.FindLocalObject(networkID);
        //apply damage
    }
}
