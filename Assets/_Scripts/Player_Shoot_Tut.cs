﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Player_Shoot_Tut : NetworkBehaviour
{
    public GameObject hitEffect;
    public Transform firstPersonCharacter;
    private RaycastHit hit;
    private int damage = 20;
   

   

 

    void Update()
    {
        if (isLocalPlayer && Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }   
    }   


    void Shoot()
    {
        print("Entro en shoot");
        if (Physics.Raycast(firstPersonCharacter.transform.position, firstPersonCharacter.transform.forward, out hit))
        {
            print("Lanzo ")
            if (hit.transform.CompareTag("Level"))
            {
                Quaternion hitAngle = Quaternion.LookRotation(hit.normal);

                CmdSpawnHitPrefab(hit.point, hitAngle);
            }
            

            if (hit.transform.CompareTag("Player"))
            {
                //Disparando a otro player
                CmdApplyDamageOnServer(hit.transform.GetComponent<NetworkIdentity>().netId);
            }
        }
    }

   

    [Command]
    void CmdSpawnHitPrefab(Vector3 pos, Quaternion rot)
    {
        print("Estoy en el Command");
        GameObject hitEffectGo = (GameObject)Instantiate(hitEffect, pos, rot);      //Spawnea en el server
        NetworkServer.Spawn(hitEffectGo);                                           //Spawnea en todo el network (los clientes supongo)
    }

    [Command]
    void CmdApplyDamageOnServer(NetworkInstanceId networkID)
    {
        GameObject hitPlayerGo = NetworkServer.FindLocalObject(networkID);
        //apply damage
    }
}