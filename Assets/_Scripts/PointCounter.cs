﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class PointCounter : NetworkBehaviour
{
    [SyncVar] public int points;
    public Text txtPointCounter;

    void Start()
    {
        points = 0;
    }

    void Update()
    {
        RpcUpdateText();

    }

    [ClientRpc]
    void RpcUpdateText()
    {
        txtPointCounter.text = points.ToString();
    }


}
