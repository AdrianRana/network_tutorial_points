﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;

public class Player_Network : NetworkBehaviour
{
    public GameObject firstPersonCharacter;
    public GameObject[] characterModel;

    public override void OnStartLocalPlayer()
    {
        Debug.Log("entra en start");
        GetComponentInChildren<FirstPersonController>().enabled = true;
        firstPersonCharacter.SetActive(true); 

        foreach(GameObject go in characterModel)
        {
            go.SetActive(false);
        }
    }
}
